import { VEprop } from './../configs.js';

export default function(attrs) {

	// First check if we have an url to make the call to.
    if(!this[VEprop].url) return console.error('No api url given.');

    if(!this.id) return console.error('Model does not have an id. See https://www.npmjs.com/package/vue-eloquent for more info.');

    return new Promise((resolve, reject) => {
        axios.get(`${this[VEprop].url}${this.id}`, attrs)
            .then(resp => {
                this.assign(this.parse ? this.parse(resp) : resp.data);
                resolve(resp);
            }, error => reject(error));
    });
}