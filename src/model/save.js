import { VEprop } from './../configs.js';

export default function(attrs, _options) {

	// First check if we have an url to make the call to.
    if(!this[VEprop].url) return console.error('No api url given.');

    if(attrs) this.assign(attrs);
    
    // if(!this.id) return console.error('Model does not have an id, use .create instead.');
    if(this.id) {
        return this.update(attrs, _options);
    } else {
        return new Promise((resolve, reject) => {
            axios.post(`${this[VEprop].url}`, this)
                .then(resp => {
                    this.assign(this.parse ? this.parse(resp) : resp.data);

                    // push the model into the collection.
                    if(this[VEprop].collection) this[VEprop].collection().push(this)

                    resolve(resp);
                }, error => reject(error));
        });
    }

}