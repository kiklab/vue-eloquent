import { VEprop } from './../configs.js';

export default function() {

	// First check if we have an url to make the call to.
    if(!this[VEprop].url) return console.error('No api url given.');

    // Save the position in case we get back an error from the server.
    var positionInCollection = this[VEprop].collection
        ? this[VEprop].collection().findIndex({ id: this.id })
        : false;

    // Remove it before we make the call, for quick user feedback ux reasons.
    if(this[VEprop].collection) this[VEprop].collection().remove(this.id);

    return new Promise((resolve, reject) => {
        axios.delete(`${this[VEprop].url}${this.id}`)
            .then(resp => {
                resolve(resp);
            }, error => {
                // put back the model, because it might not have been deleted on the server
                if(positionInCollection) this[VEprop].collection().add(this, positionInCollection);
                reject(error);
            });
    });
}