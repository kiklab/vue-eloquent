import { VEprop } from './../configs.js';

export default function(attrs, _options) {

    var options = {
        silent: false,
        ..._options
    };
    
	// First check if we have an url to make the call to.
    if(!this[VEprop].url) return console.error('No api url given.');

    if(attrs) this.assign(attrs);
    
    if(!this.id) return console.error('Model does not have an id, use .create instead.');

    return new Promise((resolve, reject) => {
        axios.put(`${this[VEprop].url}${this.id}`, this)
            .then(resp => {
                // if(!options.silent) this.assign(this[VEprop].parse ? this[VEprop].parse(resp) : resp.data);
                resolve(resp);
            }, error => reject(error));
    });
}