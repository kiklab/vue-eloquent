require('./../bootstrap');

import fetch from './model/fetch';
import update from './model/update';
import remove from './model/remove';
import save from './model/save';

import { VEprop } from './configs.js';

var assign = function(item) {

    // First set the defaults
    if(this[VEprop].props) {
        Object.keys(this[VEprop].props).forEach(prop => {
            switch(typeof this[VEprop].props[prop]) {
                case 'object':
                    if(typeof this[VEprop].props[prop].default !== 'undefined') this.Vue.set(this, prop, this[VEprop].props[prop].default);
                    break;
                default:
                    // console.log('default', prop);
            }
        });
    }


    Object.keys(item).forEach(prop => {
        // 1. Validate the prop.
        // 2. Parse the type of the prop (number, object).

        var ignores = ['remove', 'update', 'save', 'fetch'];
        if(ignores.includes(prop)) {
            console.error(`Cannot set ${prop}, because it is a reserved property in VueEloquent.`);
        } else {
            switch(typeof item[prop]) {
                case 'function':
                    // maybe check if it's already set?
                    // this[prop] = item[prop].bind(this);
                    this.Vue.set(this, prop, item[prop].bind(this));
                    break;
                default:
                    // maybe check if it's already set? 
                    this.Vue.set(this, prop, item[prop]);
            }
        }
    });
}

var config = function(config) {

    var accept = ['props', 'methods', 'url', 'collection', 'default'];

    Object.keys(config).forEach(prop => {
        if(accept.includes(prop)) {
            switch(prop) {
                case 'methods':
                    assign.apply(this, [config.methods])
                    break;
                case 'collection':
                    this[VEprop].collection = config.collection;
                    break;
                case 'default':
                    var defaults = config.default();
                    assign.apply(this, [defaults]);
                    this[VEprop].default = config.default;
                    break;
                case 'props':
                    this[VEprop].props = config.props;
                    /*
                    customProp: {
                        type: Boolean,
                        default: false
                    }
                    */
                    // add props validation and defaults
                    break;
                case 'url':
                    this[VEprop].url = config.url.substring(config.url.length - 1) === '/' ? config.url : config.url + '/';
                    break;
                default:
                    //
            }
        }
    });

}

export default class Model extends Object {

    constructor(item={}, configs, Vue) {

        super({
            __proto__: {
                fetch: fetch,
                save: save,
                remove: remove,
                update: update,
                assign: assign,
                Vue: Vue,
            }
        });

        this[VEprop] = {};

        if(configs) config.apply(this, [configs]);
        assign.apply(this, [item, Vue]);
        return this;
    }

}