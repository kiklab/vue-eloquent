import { VEprop } from './../configs.js';

export default function(_options={}) {
    var options = {
        ..._options
    };
    var url = options.url || this[VEprop].url;
    if(this.pagination.current_page > 1 && _options.current_page === undefined) options.page = this.pagination.current_page;
    if(!options.stick) this.empty();
    if(!url) console.error('no url given.');
    if(url && !this[VEprop].fetching) return new Promise((resolve, reject) => {
        this[VEprop].fetching = true;
        axios.get(url, { params: options })
            .then(resp => {
                this[VEprop].fetching = false;
                
                // http://backbonejs.org/#Collection-parse
                if(this[VEprop].parse) {
                    this[VEprop].parse(resp).forEach(this.add.bind(this));
                } else {
                    if(resp.data.data && !isNaN(resp.data.data.length)) {
                        // If pagination used in laravel controller.
                        resp.data.data.forEach(this.add.bind(this));
                        this.parsePagination(resp.data);
                    } else if(resp.data.results && !isNaN(resp.data.results.length)) {
                        resp.data.results.forEach(this.add.bind(this));
                    } else if(!isNaN(resp.data.length)) {
                        resp.data.forEach(this.add.bind(this));
                    } else {
                        console.error('Could not parse the server response.', resp);
                    }
                }

                resolve(resp);
            })
            .catch(error => {
                this[VEprop].fetching = false;
                if(this[VEprop].onError) this[VEprop].onError(error);
                if(this[VEprop].onErrorCollection) this[VEprop].onErrorCollection(error);
                reject(error);
            });
    });
}