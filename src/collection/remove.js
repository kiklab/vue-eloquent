export default function(id) {
    var index = -1;
    switch(typeof id) {
        case 'object':
            var found = this.find(item => {
                var maps = Object.keys(id).map(key => {
                    return item[key] === id[key];
                });
                return !maps.includes(false);
            });
            if(found) index = this.map(item => item.id).indexOf(found.id);
            break;
        // case 'string':
        //     index = this.map(item => item.id).indexOf(id);
        //     break;
        // case 'number':
        //     index = this.map(item => item.id).indexOf(id);
        //     break;
        default:
            index = this.map(item => item.id).indexOf(id);
    }
    if(index !== -1) {
        this.splice(index, 1);
        if(this.pagination.total && this.pagination.total > 0) this.pagination.total--;
    }
}