export default function(id, _options) {

    return this.find(item => {
        return item.id === id;
    });

}