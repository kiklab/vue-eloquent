import { VEprop } from './../configs.js';

export default function(id, _options) {
    var options = {
        ..._options
    };
    if(!this[VEprop].url) console.error('no url given.');
    if(this[VEprop].url && !this[VEprop].fetching) return new Promise((resolve, reject) => {
        this[VEprop].fetching = true;
        axios.get(this[VEprop].url + id, { params: options })
            .then(resp => {
                this[VEprop].fetching = false;
                var model = this.add(resp.data);
                resolve(model);
            }).catch(error => {
                this[VEprop].fetching = false;
                if(this[VEprop].onError) this[VEprop].onError(error);
                if(this[VEprop].onErrorCollection) this[VEprop].onErrorCollection(error);
                reject(error);
            });
    });
}