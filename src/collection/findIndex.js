// export default function(...attrs) {
export default function(attrs) {
    let index = -1;
    for (let i = this.length - 1; i >= 0; i--) {

    	let item = this[i];

        if(
            Object.keys(attrs)
                .map(key => attrs[key] === item[key])
                .every(v => v === true)
        ) {
            index = i;
            break;
        }      
    }
    return index;
}