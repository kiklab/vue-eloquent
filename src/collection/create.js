import { VEprop } from './../configs.js';

export default function(attrs) {
    if(this[VEprop].url) return new Promise((resolve, reject) => {
        axios.post(`${this[VEprop].url}`, attrs)
            .then(resp => {
            	this.add(this.parseModel ? this.parseModel(resp) : resp.data);
                resolve(resp);
            })
            .catch(error => {

                this[VEprop].fetching = false;
                if(this[VEprop].onError) this[VEprop].onError(error);
                if(this[VEprop].onErrorCollection) this[VEprop].onErrorCollection(error);

                reject(error);
            });
    });
}