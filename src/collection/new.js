import { VEprop } from './../configs.js';

export default function(item) {

    /*
    Check if there is a default set, otherwise just an empty object.
    */
    // var item = {};

    var model = new this[VEprop].model(item, {
        url: this[VEprop].url,
        collection: function() {
            return this;
        }.bind(this)
    }, this[VEprop].Vue);

    /*
	Could also be achieved like this:
	Would that be better?
    this.Vue.set(this, position ? position : this.length, model);
    */
    return model;
}