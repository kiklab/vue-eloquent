import { VEprop } from './../configs.js';

export default function(item, position = null) {
	
    if(!item.id) {
        // Should we give an error, or just send it to create?
        console.error('No id given');
    }

    var model = item instanceof this[VEprop].model
            ? item
            : new this[VEprop].model(item, {
                url: this[VEprop].url,
                collection: function() {
                    return this;
                }.bind(this)
            }, this[VEprop].Vue);

    /*
    Push the model into the arrary. 
    Check if we would like to push it at a specific position, 
    if so -> use splice.
    */
    if(this.findIndex({ id: item.id }) === -1) {
        position !== null
            ? this.splice(position, 0, model)
            : this.push(model);
    }
    /*
	Could also be achieved like this:
	Would that be better?
    this.Vue.set(this, position ? position : this.length, model);
    */
    return model;
}