import { VEprop } from './../configs.js';

export default function(searchTerm) {

    var options = {
        props: [],
        searchTerm: '',
        server: false,
    };


    if(typeof searchTerm === 'string') options.searchTerm = searchTerm;
    if(typeof searchTerm === 'object') options = {
        ...searchTerm
    }

    if(options.server) {

        // make server request
        var url = this[VEprop].url + 'search?search-term=' + options.searchTerm;

        // The searchTerm prop should be removed, because now we are giving searchTerm and search-term
        return new Promise((resolve, reject) => {
            this[VEprop].fetching = true;

            // Perhaps we should add on options wheter this should be a POST or GET
            axios.get(url, { params: options })
                .then(resp => {
                    this[VEprop].fetching = false;
                    this.empty();
                    // Hier moet nog een parse optie komen 
                    // http://backbonejs.org/#Collection-parse
                    if(this[VEprop].parse) {
                        this[VEprop].parse(resp).forEach(this.add.bind(this));
                    } else {
                        if(resp.data.data && !isNaN(resp.data.data.length)) {
                            // If pagination used in laravel controller.
                            resp.data.data.forEach(this.add.bind(this));
                            this.parsePagination(resp.data);
                        } else if(resp.data.results && !isNaN(resp.data.results.length)) {
                            resp.data.results.forEach(this.add.bind(this));
                        } else if(!isNaN(resp.data.length)) {
                            resp.data.forEach(this.add.bind(this));
                        } else {
                            console.error('Could not parse the server response.', resp);
                        }
                    }
                    resolve(resp);
                }).catch(error => {
                    this[VEprop].fetching = false;
                    if(this[VEprop].onError) this[VEprop].onError(error);
                    if(this[VEprop].onErrorCollection) this[VEprop].onErrorCollection(error);
                    reject(error);
                });
        });

    } else {

        // This is inconsistent with the above 'server' option. Shouldn't this also return a promise? 
        return this.filter(model => {
            var matches = [];
            var ignores = [VEprop];
            Object.keys(model).forEach(prop => {
                if(ignores.includes(prop)) return;
                if(options.props.length > 0) if(!options.props.includes(prop)) return;
                matches.push(typeof model[prop] === 'string'
                    ? model[prop].toLowerCase().indexOf(options.searchTerm.toLowerCase()) !== -1
                    : false
                );
            });
            return matches.indexOf(true) !== -1;
        });
        
    }
}