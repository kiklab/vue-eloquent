export default function(attrs, value) {
	switch(typeof attrs) {
		case 'string': return this.filter(model => model[attrs] === value);
		case 'function': return this.filter(attrs);
		/*
		This is the default.
		case 'object': return this.filter(model => !Object.keys(attrs).map(prop => model[prop] === attrs[prop]).includes(false));
		*/
		default: return this.filter(model => !Object.keys(attrs).map(prop => model[prop] === attrs[prop]).includes(false));
	}
}