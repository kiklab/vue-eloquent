import { VEprop } from './../configs.js';


export default function(order, direction) {

    var orderBy = function(a, b) {
        return a[order]-b[order];
    }

    return direction === 'desc'
        ? this.sort(orderBy).reverse()
        : this.sort(orderBy);
}