require('./../bootstrap');
import Model from './Model.js';

import add from './collection/add.js';
import create from './collection/create.js';
import findIndex from './collection/findIndex.js';
import get from './collection/get.js';
import remove from './collection/remove.js';
import fetch from './collection/fetch.js';
import fetchId from './collection/fetchId.js';
import empty from './collection/empty.js';
import where from './collection/where.js';
import search from './collection/search.js';
import newModel from './collection/new.js';
import parsePagination from './collection/parsePagination.js';
import Vue from 'vue';

import { VEprop } from './configs.js';

var setCollectionConfig = function(attrs) {

    // let's check here which configs we accept.
    var acceptedConfigs = [
        'url',
        'model',
        'parse',
        'parsePagination',
        'collection',
        'methods',
        'onError',
        'onErrorCollection',
    ];
    
    if(attrs) Object.keys(attrs).forEach(attr => {
        
        // Check if this attr is an accepted configuration property / method.
        if(acceptedConfigs.includes(attr)) {

            // Set the base url for this collection
            if(attr === 'url') {
                return this[VEprop].url = attrs.url.substring(attrs.url.length - 1) === '/'
                    ? attrs.url
                    : attrs.url + '/';
            }

            // Relate the model with this collection. 
            if(attr === 'model') {
                this[VEprop].model = attrs.model;
                this[VEprop].parseModel = attrs.model.prototype.getModelParse();
                return;
            }

            // Add custom methods onto this collection.
            if(attr === 'methods') return Object.keys(attrs.methods).forEach(method => {
                this[method] = attrs.methods[method];
            });

            // Add parse response method on this collection.
            if(attr === 'parse') return this[VEprop][attr] = attrs[attr].bind(this);

            // Add the error handler
            if(attr === 'onError') return this[VEprop][attr] = attrs[attr];

            // Add the collection error handler
            if(attr === 'onErrorCollection') return this[VEprop][attr] = attrs[attr];

            // Non of the above? Then let's just add the attribute onto the collection.
            this[VEprop][attr] = attrs[attr];

        }
    });
 
    // If no model is given, let's use the VueEloquent default.
    if(!this[VEprop].model) this[VEprop].model = (function(config) {
        class AutoModel extends Model {
            constructor(attrs, conf) {
                super(attrs, {...conf, ...config}, Vue);
            }
            getModelParse() {
                return false;
            }
        }
        return AutoModel;
    }({
        url: this[VEprop].url,
        collection: function() {
            return this;
        }.bind(this)
    }))
        
}

export default class Collection extends Array {
    constructor(config, Vue) {
        super();

        this[VEprop] = {
            fetching: false,
            Vue: Vue
        };

        setCollectionConfig.apply(this, [config]);

        this.add = add;
        this.create = create;
        this.empty = empty;
        this.fetch = fetch;
        this.fetchId = fetchId;
        this.findIndex = findIndex;
        this.get = get;
        this.remove = remove;
        this.where = where;
        this.search = search;
        this.new = newModel;
        this.parsePagination = parsePagination;
		this.searchResults = {};
		Vue.set(this.searchResults, 'results', []);
        this.collection = function() {
            return this;
        }.bind(this)
        this.pagination = {
	    	current_page: 0,
		    first_page_url: '',
		    from: 0,
		    last_page: 0,
		    last_page_url: '',
		    next_page_url: '',
		    path: '',
		    per_page: 0,
		    prev_page_url: '',
		    to: 0,
		    total: 0,
        };

        this.model = this[VEprop].model;

        return this;
        // return this.searchResults.length > 0 ? this.searchResults : this;
    }

}
