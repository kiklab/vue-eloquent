var path = require('path');
var webpack = require('webpack');
module.exports = {
    target: 'web',
    entry: './index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        library: 'vue-eloquent',
        libraryTarget: 'commonjs2', // commonjs2, commonjs, umd, amd
        filename: 'index.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets:[ 'es2015', 'stage-0' ]
                }
            }
        ]
    },
    stats: {
        colors: true
    },
    resolve: {
        // alias: {
        //     '@': path.join(__dirname, 'src')
        // },
        extensions: ['*', '.js']
    },
    node: {
        fs: 'empty'
    },
    optimization: {
        minimize: true
    }
};
