require('./bootstrap');
import Collection from './src/Collection.js';
import Model from './src/Model.js';

export default {
	install(Vue, options={}) {

		if(options.csrf_token) window.axios.defaults.headers.common['X-CSRF-TOKEN'] = options.csrf_token;

		Vue.prototype.$model = function(config={}) {

			class M extends Model {
				constructor(attrs, conf) {
					super(attrs, {...conf, ...config}, Vue);
				}
				/*
				This method is for the collection, so it can grab the model parse. 
				The collection needs this parse method for its create method.
				*/
				getModelParse() {
					return config.parse ? config.parse : false;
				}
			}

			return M;
		}

		Vue.prototype.$collection = function(_config={}) {
			var config = {
				onError: options.onError,
				..._config
			};
			return new Collection(config, Vue);
		};

	}
}