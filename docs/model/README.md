## $model

With `$model` we can define methods on the models.

Methods with a request to the server
- [fetch](#fetch)
- [update](#update)
- [remove](#remove)

Configs
- [parse](#parse)

**Todos.vue**
``` vue {4,21-25,30}
<template>
	<div
		v-for="todo in todos"
		@click="todo.toggle">
		<i class="icon-checked" v-if="todo.status"></i>
		<i class="icon-unchecked" v-else></i>
		{{ todo.description }}
	</div>
</template>

<script>
	export default {
		mounted() {
			this.todos.fetch();
			this.todos.create({
				description: 'Have shoarma with the Avengers.',
				status: false
			})
		},
		data() {
			var Todo = this.$model({
				toggle() {
					this.status = !this.status
				}
			});

			return {
				todos: this.$collection({
					url: '/api/todos',
					model: Todo
				})
			}
		}
	}
</script>
```

## fetch
``` js
todo.fetch({
	load: 'user'
});
```
Example of controller setup with Laravel:
``` php
public function show($id, Request $request)
{
	$todo = Todo::find($id);
	if($request->has('load')) $todo->load($request->get('load'));
	return $todo;
}
```

## remove
Makes a `DELETE` request to the server with the `id` and returns a `Promise`.
`axios.delete('/api/todos/' + id)`
``` js
todo.remove()
	.then(resp => {
		/*
		Todo is automagicly removed from the collection,
		but we could give the user feedback
		with a sweet alert message for example:

		swal('Todo is deleted');

		https://sweetalert2.github.io/
		*/
	}, error => console.error);
```

## parse
In case the server gives a different response than expected, you might use the `.parse` method on the model creation as shown bellow:
**Todos.vue**
``` vue {5-8}
<script>
	export default {
		data() {
			var Todo = this.$model({
				parse(response) {
					var body = JSON.parse(response);
					return body.data;
				}
			});

			return {
				todos: this.$collection({
					url: '/api/todos',
					model: Todo
				})
			}
		}
	}
</script>
```