# Getting started!


## Install the plugin
Install the plugin in your main javascript file:
**main.js**
``` js {3,4}
import Vue from 'vue';

import VueEloquent from 'vue-eloquent';
Vue.use(VueEloquent);

// Or with a csrf token
Vue.use(VueEloquent, {
	csrf_token: 'YOUR_TOKEN'
});


```

## Create a collection
In a Vue component we can now create a "smart" collection with `this.$collection`. After this we are able to fetch the collection from the server with `this.todos.fetch()`.

**Todos.vue**
``` vue {10,14-16}
<template>
	<div v-for="todo in todos" :key="todo.id">
		{{ todo.description }}
	</div>
</template>

<script>
	export default {
		mounted() {
			this.todos.fetch();
		},
		data() {
			return {
				todos: this.$collection({
					// We need this to know where the api calls should go.
					url: '/api/todos'
				})
			}
		}
	}
</script>
```

## Update and remove $models
Because a `$collection` is filled with super smart `$models` we can now do stuff like this:

**Todos.vue**
``` vue {4,8}
<template>
	<div>
		<div v-for="todo in todos" :key="todo.id">
			<span @click="todo.update({ status: !todo.status })">
				{{ todo.status ? 'O' : 'X' }}
			</span>
			<span>{{ todo.description }}</span>
			<button @click.stop="todo.remove">X</button>
		</div>
	</div>
</template>

<script>
	export default {
		mounted() {
			this.todos.fetch();
		},
		data() {
			return {
				todos: this.$collection({
					url: '/api/todos'
				})
			}
		}
	}
</script>
```
In the above code `todo.update` will make an `axios.put` call with the todo as payload. Remove will make an `axios.delete` call width the id.

## Create a new model
**Todos.vue**
``` vue {25-29}
<template>
	<div>
		<input
			type="text"
			placeholder="New todo"
			@keyup.enter="createTodo"
			v-model="newTodo" />
		<div v-for="todo in todos" :key="todo.id">
			<span @click="todo.update({ status: !todo.status })">
				{{ todo.status ? 'O' : 'X' }}
			</span>
			<span>{{ todo.description }}</span>
			<button @click.stop="todo.remove">X</button>
		</div>
	</div>
</template>

<script>
	export default {
		mounted() {
			this.todos.fetch();
		},
		methods: {
			createTodo() {
				this.todos
					.create({ description: this.newTodo })
					.then(resp => {
						this.newTodo = '';
					});
			}
		},
		data() {
			return {
				todos: this.$collection({
					url: '/api/todos'
				}),
				newTodo: ''
			}
		}
	}
</script>
```