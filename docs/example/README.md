---
title: Page 1
# We can even add meta tags to the page! This sets the keywords meta tag.
# <meta name="keywords" content="my SEO keywords"/>
actionText: Get Started →
actionLink: /guide/
meta:
  - name: keywords
  - content: my SEO keywords
footer: MIT Licensed | Copyright © 2018-present Evan You
---

`var heros = [{ name: 'Iron man'}]`
<ul>
	<li v-for="hero in [{ name: 'iron man'}]">{{ hero.name }}</li>
</ul>

