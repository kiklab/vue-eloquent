---
title: VueEloquent
# We can even add meta tags to the page! This sets the keywords meta tag.
# <meta name="keywords" content="my SEO keywords"/>
actionText: Get Started →
meta:
  - name: keywords
  - content: my SEO keywords
footer: MIT Licensed | Copyright © 2018-present Evan You
---

[[toc]]

# What is VueEloquent?

A Vue plugin to make your code yet again a little more readable with just a handfull of coding lines.

We have started this library because we <3 [Laravel](https://laravel.com/), <3 good ol' [Backbone.js](http://backbonejs.org/) and mostly [Vue.js](https://vuejs.org). And this is the plugin for vue that we were missing to keep our code even cleaner. If you are familiar with these frameworks, than you probably recognize some of the patterns.

If you are enjoying this plugin, or if you have suggestions for improvements/new features, let us know! [kiklab.nl](http://kiklab.nl/)

## What does it look like?
This is just a small taste of what `vue-eloquent` can do.

**Todos.vue** 
``` vue {9,13,21,25-29,34-36}
<template>
	<div>
		<input
			type="text"
			placeholder="New todo"
			@keyup.enter="createTodo"
			v-model="newTodo" />
		<div v-for="todo in todos" :key="todo.id">
			<span @click="todo.update({ status: !todo.status })">
				{{ todo.status ? 'O' : 'X' }}
			</span>
			<span>{{ todo.description }}</span>
			<button @click.stop="todo.remove">X</button>
		</div>
	</div>
</template>

<script>
	export default {
		mounted() {
			this.todos.fetch();
		},
		methods: {
			createTodo() {
				this.todos
					.create({ description: this.newTodo })
					.then(resp => {
						this.newTodo = '';
					});
			}
		},
		data() {
			return {
				todos: this.$collection({
					url: '/api/todos'
				}),
				newTodo: ''
			}
		}
	}
</script>
```