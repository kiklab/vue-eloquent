module.exports = {
	dest: 'public',
	base: '/vue-eloquent/',
	title: 'VueEloquent Docs',
	description: 'VueEloquent Documents',
	themeConfig: {
		nav: [
			{ text: 'Home', link: '/' },
			{ text: 'Kiklab.nl', link: 'https://kiklab.nl' },
		],
		sidebar: [
			['/', 'Intro'],
			['/getting-started/', 'Getting started'],
			['/model/', 'Model'],
			['/collection/', 'Collection'],
			['/example/', 'HTML'],
		]
	}
}