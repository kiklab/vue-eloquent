## $collection


Methods with a request to the server
- [fetch](#fetch)
- [get](#get)
- [create](#create)

Collection configs
- [parse](#parse)

Collection methods (no server interaction)
- [add](#add)
- [remove](#remove)
- [empty](#empty)
- [where](#where)
- [orderBy](#orderBy)


## configurations
In case the server gives a different response than expected, you might use the `.parse` method on the collection.

**Todos.vue**
``` vue {7-10}
...
<script>
	export default {
		data() {
			return {
				todos: this.$collection({
					url: '/api/todos',
					parse(response) {
						var body = JSON.parse(response);
						return body.results;
					}
				})
			}
		}
	}
</script>
```

## fetch
``` js
this.todos.fetch();
```
Or if we would like to send some extra parameters with the request:
``` js
this.todos.fetch({
	page: 2,
	with: 'user'
});
```
The collection can handle two types of json server responses:
``` json
[
	{ id: 1, description: 'Make pancakes', status: 0 },
	{ id: 2, description: 'Buy flower', status: 1 },
	{ id: 3, description: 'Brew coffee', status: 0 }
]
```
Or with pagination (This is how [Laravel](https://laravel.com/) <3 returns a paginated query)
``` json
{
	"current_page": 1,
	"data": [
		{ id: 1, description: 'Make pancakes', status: 0 },
		{ id: 2, description: 'Buy flower', status: 1 },
		{ id: 3, description: 'Brew coffee', status: 0 },
		...
	],
	"first_page_url": "http://kiklab.nl/api/todos?page=1",
	"from": 1,
	"last_page": 6,
	"last_page_url": "http://kiklab.nl/api/todos?page=6",
	"next_page_url": "http://kiklab.nl/api/todos?page=2",
	"path": "http://kiklab.nl/api/todos",
	"per_page": 50,
	"prev_page_url": null,
	"to": 50,
	"total": 262
}
```

If you use laravel your controller might look like this:
``` php
class TodoController extends Controller {
	public function(Request $request)
	{
		$per_page = $request->get('per_page', 50);
		$todos = Todo::orderBy('created_at');
		if($request->has('with')) $todos->with($request->get('with'));
		return $todos->paginate($per_page);
	}
}
```

## get

The `.get()` method looks for a model with the given id in the collection.
``` js
// If you want to fetch the id from your route for example
var id = this.$route.params.id;
this.todos.get(id); // returns the model with corresponding id. 
```


## create
``` js
this.todos.create({
	description: 'Make coffee',
	status: 0
});
```


Collection methods

## add
Adds a new `$model` to the collection. You can pass it an object, or model instance.
``` js
this.todos.add({
	id: 2,
	description: 'Make coffee',
	status: 0
});

var Todo = this.$model;

var task = new Todo({
	id: 2,
	description: 'Make coffee',
	status: 0
});

this.todos.add(task);
```

## remove
Simply removes the model from the collection.
``` js
this.todos.remove(id);
this.todos.remove({ id: 2, description: '...' })
```

## empty
This will empty the collection.
``` js
this.todos.empty();
```

## where
Returns an array of matched models.
``` js
// pass through an object with one or more conditions.
this.todos.where({ status: 1 });
this.todos.where({ status: 1, description: 'Buy milk' });

// Or with an attribute and value.
this.todos.where('status', 1);

// Or a function.
this.todos.where(item => {
	return item.status === true && item.description === 'Buy milk';
});
```

## orderBy
Returns a sorted array.
``` js
this.todos.orderBy('status');
```