import Vue from 'vue';
import VueEloquent from '../../../index.js';
Vue.use(VueEloquent);

import mockAxios from 'axios';

it('Fetches model from server', async () => {
	
	mockAxios.get.mockImplementationOnce(() => Promise.resolve({
		data: {
			id: 1,
			description: 'Make pie'
		}
	}))

	const vm = new Vue();
	var todoModel = vm.$model({
		url: '/api/todos'
	});
	var todo = new todoModel({ id: 1 });
	var response = await todo.fetch();

	expect(typeof todo.save).toBe('function');
	expect(typeof todo.update).toBe('function');
	expect(typeof todo.remove).toBe('function');
	expect(todo.description).toBe('Make pie');
	expect(mockAxios.get).toHaveBeenCalledTimes(1);
	expect(mockAxios.get).toHaveBeenCalledWith("/api/todos/1", undefined);

});