import Vue from 'vue';
import VueEloquent from '../../../index.js';
Vue.use(VueEloquent);

import mockAxios from 'axios';

it('makes delete request to server', async () => {
	
	mockAxios.delete.mockImplementationOnce(() => Promise.resolve({
		data: {
			id: 1,
			description: 'Make pie'
		}
	}))

	const vm = new Vue();
	var todoModel = vm.$model({
		url: '/api/todos'
	});
	var todo = new todoModel({ id: 1 });
	var response = await todo.remove();

	expect(mockAxios.delete).toHaveBeenCalledTimes(1);
	expect(mockAxios.delete).toHaveBeenCalledWith("/api/todos/1");

});