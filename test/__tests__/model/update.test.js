import Vue from 'vue';
import VueEloquent from '../../../index.js';
Vue.use(VueEloquent);

import mockAxios from 'axios';

it('makes put request to server', async () => {
	
	mockAxios.put.mockImplementationOnce(() => Promise.resolve({
		data: {
			id: 1,
			description: 'Make a test with the save method.'
		}
	}))


	const vm = new Vue();
	var todoModel = vm.$model({
		url: '/api/todos'
	});
	var todo = new todoModel({ id: 1, description: 'Make a test with the save method.' });
	var response = await todo.update();
	expect(mockAxios.put).toHaveBeenCalledTimes(1);
	expect(mockAxios.put).toHaveBeenCalledWith(
		"/api/todos/1",
		{
			__VE__: {"url": "/api/todos/"},
			description: "Make a test with the save method.",
			id: 1
		}
	);

});