import Vue from 'vue';
import VueEloquent from '../../../index.js';
Vue.use(VueEloquent);
import { VEprop } from '../../../src/configs.js';

import mockAxios from 'axios';

it('makes post request to server', async () => {
	
	mockAxios.post.mockImplementationOnce(() => Promise.resolve({
		data: {
			id: 1,
			description: 'Make a test with the save method.'
		}
	}))


	const vm = new Vue();
	var todoModel = vm.$model({
		url: '/api/todos'
	});
	var todo = new todoModel({ description: 'Make a test with the save method.' });
	var response = await todo.save();
	expect(mockAxios.post).toHaveBeenCalledTimes(1);
	expect(mockAxios.post).toHaveBeenCalledWith(
		"/api/todos/",
		{
			__VE__: {"url": "/api/todos/"},
			description: "Make a test with the save method.",
			id: 1
		}
	);

});

it('If a new model instance is created from the collection object, it is added to the collection after saving it for the first time.', async () => {

	mockAxios.post.mockImplementationOnce(() => Promise.resolve({
		data: {
			id: 1,
			description: 'Have tea with Thor.'
		}
	}))

	mockAxios.put.mockImplementationOnce(() => Promise.resolve({
		data: {
			id: 1,
			description: 'Have tea with Thor and the Hulk.'
		}
	}))

	const vm = new Vue();
	var todoCollection = vm.$collection({
		url: '/api/todos'
	});

	var todo = todoCollection.new();
	todo.description = 'Have tea with Thor.';
	todo.save();
	expect(mockAxios.post).toHaveBeenCalledTimes(2);

	// Fake the id
	todo.id = 1;

	todo.description = 'Have tea with Thor and the Hulk.';
	todo.save();
	expect(mockAxios.put).toHaveBeenCalledTimes(1);

});