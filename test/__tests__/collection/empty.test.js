import Vue from 'vue';
import VueEloquent from '../../../index.js';
Vue.use(VueEloquent);

import mockAxios from 'axios';

test('adds model to collection', async () => {

	mockAxios.get.mockImplementationOnce(() => Promise.resolve({
		data: {
			results: [
				{
					id: 1,
					description: 'Make pie'
				},
				{
					id: 2,
					description: 'Make coffee'
				},
				{
					id: 3,
					description: 'Call mom'
				},
				{
					id: 4,
					description: 'Drink wine'
				}
			]
		}
	}))

	const vm = new Vue();
	var todos = vm.$collection({
		url: '/api/todos'
	});

	await todos.fetch();

	expect(todos.length).toBe(4);

	todos.empty();

	expect(todos.length).toBe(0);
	
});