import Vue from 'vue';
import VueEloquent from '../../../index.js';
Vue.use(VueEloquent);

import mockAxios from 'axios';

it('adds model to collection', async () => {
	
	mockAxios.get.mockImplementationOnce(() => Promise.resolve({
		data: {
			results: [
				{
					id: 1,
					description: 'Make pie'
				},
				{
					id: 2,
					description: 'Make coffee'
				}
			]
		}
	}))

	const vm = new Vue();
	var todos = vm.$collection({
		url: '/api/todos'
	});
	var response = await todos.fetch();

	expect(todos.length).toBe(2);
	expect(mockAxios.get).toHaveBeenCalledTimes(1);
	expect(mockAxios.get).toHaveBeenCalledWith(
		"/api/todos/",
		{"params": {}}
	);

});