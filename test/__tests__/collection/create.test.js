import { VEprop } from '../../../src/configs.js'
import Vue from 'vue';
import VueEloquent from '../../../index.js';
// Vue.use(VueEloquent);

import mockAxios from 'axios';

it('adds model to collection', async () => {

	Vue.use(VueEloquent);

	var newItem = {
		id: 1,
		description: 'just a jest test'
	};
	
	mockAxios.post.mockImplementationOnce(() => Promise.resolve({
		data: newItem
	}))

	const vm = new Vue();
	var todos = vm.$collection({
		url: '/api/todos'
	});
	var response = await todos.create(newItem);

	expect(todos.length).toBe(1);
	expect(mockAxios.post).toHaveBeenCalledTimes(1);
	expect(mockAxios.post).toHaveBeenCalledWith(
		"/api/todos/",
		newItem
	);

});

it('handles the error method of a collection', async () => {

	Vue.use(VueEloquent);

	mockAxios.post.mockRejectedValue(new Error('You cannot create.'));

	const vm = new Vue();
	var todos = vm.$collection({
		url: '/api/todos',
		onErrorCollection(error) {
			// console.error('error for this collection');
		},
	});

    const spy = jest.spyOn(todos[VEprop], 'onErrorCollection');

	var apiCall = todos.create({id: 1, description: 'just a jest test'})
	try {
		await apiCall;
	} catch(e) {
		// console.log(e);
	}

    expect(apiCall).rejects.toThrowError('You cannot create.');
	expect(spy).toHaveBeenCalledTimes(1);
	// expect.assertions(1);

});