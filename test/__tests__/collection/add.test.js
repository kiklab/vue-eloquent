import Vue from 'vue';
import VueEloquent from '../../../index.js';
Vue.use(VueEloquent);

import mockAxios from 'axios';

test('adds model to collection', () => {

	const vm = new Vue();
	var todos = vm.$collection({
		url: '/api/todos'
	});

    todos.add({
        id: 1,
        description: 'just a jest test'
    })


	expect(todos.length).toBe(1);
	
});

test('adds a model to the collection at a position', async () => {
    
    mockAxios.get.mockImplementationOnce(() => Promise.resolve({
        data: {
            results: [
                {
                    id: 1,
                    description: 'Make pie'
                },
                {
                    id: 2,
                    description: 'Make coffee'
                },
                {
                    id: 3,
                    description: 'Make beer'
                }
            ]
        }
    }))

    const vm = new Vue();
    var todos = vm.$collection({
        url: '/api/todos'
    });
    var response = await todos.fetch();

    todos.add({
        id: 4,
        description: 'just a jest test'
    }, 2);

    expect(todos[2].id).toBe(4);

    todos.add({
        id: 5,
        description: '0 should still be accepted'
    }, 0);

    expect(todos[0].id).toBe(5);

    todos.add({
        id: 6,
        description: 'Goes to the end.'
    });

    expect(todos[todos.length-1].id).toBe(6);

});