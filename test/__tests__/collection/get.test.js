import Vue from 'vue';
import VueEloquent from '../../../index.js';
Vue.use(VueEloquent);

import mockAxios from 'axios';

it('adds model to collection', async () => {
	
	mockAxios.get.mockImplementationOnce(() => Promise.resolve({
		data: {
			results: [
				{
					id: 1,
					description: 'Make pie'
				},
				{
					id: 2,
					description: 'Make coffee'
				}
			]
		}
	}))

	const vm = new Vue();
	var todos = vm.$collection({
		url: '/api/todos'
	});
	var response = await todos.fetch();

	var todo = todos.get(2);

	expect(todo.description).toBe('Make coffee');
	expect(typeof todo.update).toBe('function');
	expect(typeof todo.save).toBe('function');
	expect(typeof todo.remove).toBe('function');

});