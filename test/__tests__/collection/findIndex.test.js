import Vue from 'vue';
import VueEloquent from '../../../index.js';
Vue.use(VueEloquent);

import mockAxios from 'axios';

it('adds model to collection', async () => {
	
	mockAxios.get.mockImplementationOnce(() => Promise.resolve({
		data: {
			results: [
				{
					id: 1,
					description: 'Make pie'
				},
				{
					id: 2,
					description: 'Make coffee'
				},
				{
					id: 3,
					description: 'Call mom'
				},
				{
					id: 4,
					description: 'Drink wine'
				}
			]
		}
	}))

	const vm = new Vue();
	var todos = vm.$collection({
		url: '/api/todos'
	});

	await todos.fetch();

	var todo2 = await todos.findIndex({ id: 2, description: 'Make coffee' });
	var todo3 = await todos.findIndex({ id: 3 });
	var todo4 = await todos.findIndex({ description: 'Drink wine' });

	expect(todo2).toBe(1);
	expect(todo3).toBe(2);
	expect(todo4).toBe(3);

});