import Vue from 'vue';
import VueEloquent from '../../../index.js';
Vue.use(VueEloquent);

import mockAxios from 'axios';

it('makes a new instance of the model of this collection', async () => {
	
	const vm = new Vue();

	const Todo = vm.$model({
	    methods: {
	        toggle() {
	            this.status = !this.status;
	        },
	    },
	    default() {
	    	return {
	    		description: '',
	    		status: false,
	    	}
	    }
	});

	var todos = vm.$collection({
		url: '/api/todos',
		model: Todo
	});
	var newTodo = todos.new({ foo: 'bar' });

	expect(newTodo.foo).toBe('bar');
	expect(newTodo.status).toBe(false);
	expect(newTodo.description).toBe('');

});