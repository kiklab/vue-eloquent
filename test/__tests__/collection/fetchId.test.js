import Vue from 'vue';
import VueEloquent from '../../../index.js';
Vue.use(VueEloquent);

import mockAxios from 'axios';

it('adds model to collection', async () => {
	
	mockAxios.get.mockImplementationOnce(() => Promise.resolve({
		data: {		
			id: 1,
			description: 'Make pie'
		}
	}));

	const vm = new Vue();
	var todos = vm.$collection({
		url: '/api/todos'
	});
	var response = await todos.fetchId(1);

	expect(todos.length).toBe(1);
	expect(mockAxios.get).toHaveBeenCalledTimes(1);
	expect(mockAxios.get).toHaveBeenCalledWith(
		"/api/todos/1",
		{"params": {}}
	);

});