'use strict';

const todos = [
  { id: 1, description: 'Make pie'},
  { id: 2, description: 'Drink wine'},
];

export default {
  get: jest.fn(url => {
    return Promise.resolve({data: {
      id: 1,
      description: 'Make pie'
    }})
  }),
  post: jest.fn(item => {
    return Promise.resolve({ data: {
      id: 1,
      description: 'Bla bla'
    }})
  }),
  put: jest.fn(item => {
    return Promise.resolve({ data: {
      id: 1,
      description: 'Bla bla'
    }})
  }),
  delete: jest.fn(item => {
    return Promise.resolve({ data: {
      id: 1,
      description: 'Bla bla'
    }})
  }),
  defaults: {
    headers: {
      common: {},
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
  }
}